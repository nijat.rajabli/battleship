from __future__ import annotations

from typing import TYPE_CHECKING

import config
from models.board import Board
from views.board_frame import BoardFrame

if TYPE_CHECKING:  # pragma: no cover
    from models.cell import Cell
    from models.ship import Ship


class ReadyBoardFrame(BoardFrame):
    def __init__(self, board: Board, **kw):
        super(ReadyBoardFrame, self).__init__(board, **kw)

    def bind_cell_events(self, cell) -> None:
        pass

    def rerender(self) -> None:
        for cell in self.board.get_cells():
            fill = self.get_cell_color(cell)
            self.canvas.itemconfig(cell.id, fill=fill)

    @staticmethod
    def get_initial_cell_color(cell: Cell) -> str:
        return config.SHIP_COLOR if cell.has_ship_part() else config.EMPTY_COLOR

    @staticmethod
    def get_cell_color(cell: Cell) -> str:
        if cell.is_targeted:
            fill = ReadyBoardFrame.get_targeted_cell_color(cell)
        elif cell.has_ship_part():
            fill = config.SHIP_COLOR
        else:
            fill = config.EMPTY_COLOR
        return fill

    @staticmethod
    def get_targeted_cell_color(cell: Cell) -> str:
        if cell.has_ship_part():
            fill = ReadyBoardFrame.get_targeted_ship_color(cell.ship)
        else:
            fill = config.MISS_COLOR
        return fill

    @staticmethod
    def get_targeted_ship_color(ship: Ship) -> str:
        if ship.health == 0:
            fill = config.DESTROYED_COLOR
        else:
            fill = config.HIT_COLOR
        return fill
