from __future__ import annotations

import tkinter as tk
from abc import ABC, abstractmethod
from tkinter import Canvas, Frame
from typing import TYPE_CHECKING, Dict, Tuple

from models.board import Board
from observer import Observer

if TYPE_CHECKING:  # pragma: no cover
    from models.cell import Cell


class BoardFrame(Observer, Frame, ABC):
    cells: Dict[int, Cell]
    canvas: Canvas

    canvas_padding_px: int = 20
    cell_size_px: int = 30

    def __init__(self, board: Board, **kw):
        super(BoardFrame, self).__init__(**kw)
        self.cells = {}
        self.board = board

    def render(self) -> None:
        self.observe(self.board, self.rerender)

        self.render_canvas()
        self.render_cells()

    def render_canvas(self) -> None:
        width, height = self.get_canvas_dimensions()
        self.canvas = Canvas(self, width=width, height=height)
        self.canvas.pack()

    def render_cells(self) -> None:
        for y in range(0, self.board.SIZE):
            for x in range(0, self.board.SIZE):
                cell = self.board.content[y][x]
                cell.id = self.render_cell(cell)
                self.index_cell(cell)
                self.bind_cell_events(cell)

    def render_cell(self, cell: Cell) -> Cell:
        fill = self.get_initial_cell_color(cell)
        rectangle_coordinates = self.get_cell_rectangle_coordinates(cell)
        return self.canvas.create_rectangle(*rectangle_coordinates,
                                            fill=fill)

    def index_cell(self, cell: Cell) -> None:
        self.cells[cell.id] = cell

    @staticmethod
    def get_canvas_dimensions() -> Tuple[int, int]:
        dim = BoardFrame.cell_size_px * Board.SIZE + BoardFrame.canvas_padding_px * 2 - 5
        return dim, dim

    @staticmethod
    def get_cell_rectangle_coordinates(cell: Cell) -> Tuple[int, int, int, int]:
        x1 = BoardFrame.canvas_padding_px + cell.x * BoardFrame.cell_size_px
        y1 = BoardFrame.canvas_padding_px + cell.y * BoardFrame.cell_size_px
        x2 = BoardFrame.canvas_padding_px + BoardFrame.cell_size_px + cell.x * BoardFrame.cell_size_px
        y2 = BoardFrame.canvas_padding_px + BoardFrame.cell_size_px + cell.y * BoardFrame.cell_size_px
        return x1, y1, x2, y2

    @staticmethod
    @abstractmethod
    def get_initial_cell_color(cell: Cell) -> str:
        pass

    @abstractmethod
    def bind_cell_events(self, cell: Cell) -> None:
        pass

    @abstractmethod
    def rerender(self) -> None:
        pass

    def get_current_cell(self) -> Cell:
        cell_id = self.canvas.find_withtag(tk.CURRENT)[0]
        return self.cells[cell_id]

    def destroy(self) -> None:
        self.stop_observing()
        super(BoardFrame, self).destroy()
