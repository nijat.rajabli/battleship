from __future__ import annotations

from tkinter import messagebox, Toplevel
from typing import TYPE_CHECKING

from observer import Observer
from views.enemy_board_frame import EnemyBoardFrame
from views.player_window import PlayerWindow

if TYPE_CHECKING:  # pragma: no cover
    from models.game import Game
    from models.player import Player


class GameWindow(Observer, Toplevel):
    game: Game
    player_1_window: PlayerWindow
    player_2_window: PlayerWindow

    def __init__(self, game: Game, **kw):
        super(GameWindow, self).__init__(**kw)
        self.game = game

    def render(self) -> None:
        self.observe(self.game.player_1, self.rerender)
        self.observe(self.game.player_2, self.rerender)

        self.withdraw()
        self.open_player_windows()

    def are_enemy_boards_shown(self) -> bool:
        return bool(self.player_1_window.enemy_board_frame or self.player_2_window.enemy_board_frame)

    def show_enemy_boards(self) -> None:
        self.show_enemy_board(self.player_1_window, self.game.player_1)
        self.show_enemy_board(self.player_2_window, self.game.player_2)

    @staticmethod
    def show_enemy_board(player_window: PlayerWindow, current_player: Player) -> None:
        player_window.enemy_board_frame = EnemyBoardFrame(
            master=player_window,
            board=current_player.enemy.board,
            current_player=current_player
        )
        player_window.enemy_board_frame.grid(row=0, column=1)
        player_window.enemy_board_frame.render()

    def rerender(self) -> None:
        if not self.are_enemy_boards_shown() and self.game.is_ready():
            self.show_enemy_boards()
        elif self.is_open() and self.game.is_over():
            self.master.play = self.ask_replay(self.game.get_winner_name())
            self.close_player_windows()
            self.destroy()
            self.quit()

    def is_open(self) -> bool:
        return self.winfo_exists()

    def open_player_windows(self) -> None:
        self.player_1_window = PlayerWindow(master=self.master, player=self.game.player_1)
        self.player_1_window.render()

        self.player_2_window = PlayerWindow(master=self.master, player=self.game.player_2)
        if self.game.player_2.is_ai_player():
            self.player_2_window.withdraw()
        self.player_2_window.render()

    def close_player_windows(self) -> None:
        self.player_1_window.destroy()
        self.player_2_window.destroy()

    def ask_replay(self, winner_name) -> bool:
        title = "Game Over! Replay?"
        message = "{} won the game! Do you want to replay?".format(winner_name)
        return messagebox.askyesno(title, message, parent=self)
