from __future__ import annotations

from typing import TYPE_CHECKING

import config
from controllers import enemy_board_controller
from models.ship import Ship
from views.board_frame import BoardFrame

if TYPE_CHECKING:  # pragma: no cover
    from models.board import Board
    from models.cell import Cell
    from models.player import Player


class EnemyBoardFrame(BoardFrame):
    current_player: Player

    def __init__(self, board: Board, current_player: Player, **kw):
        super(EnemyBoardFrame, self).__init__(board, **kw)
        self.current_player = current_player

    def bind_cell_events(self, cell: Cell) -> None:
        self.canvas.tag_bind(cell.id, "<Enter>", self.on_enter)
        self.canvas.tag_bind(cell.id, "<Leave>", self.on_leave)
        self.canvas.tag_bind(cell.id, "<Button-1>", self.left_click)

    def on_enter(self, _event=None) -> None:
        cell = self.get_current_cell()
        if self.current_player.can_target_cell(cell):
            self.canvas.config(cursor=config.TARGET_CURSOR)

    def on_leave(self, _event=None) -> None:
        self.canvas.config(cursor=config.DEFAULT_CURSOR)

    def left_click(self, _event=None) -> None:
        self.canvas.config(cursor=config.DEFAULT_CURSOR)
        cell = self.get_current_cell()
        enemy_board_controller.left_click(cell, self.current_player)

    def rerender(self) -> None:
        for cell in self.board.get_cells():
            fill = self.get_cell_color(cell)
            self.canvas.itemconfig(cell.id, fill=fill)

    @staticmethod
    def get_initial_cell_color(cell: Cell) -> str:
        return config.EMPTY_COLOR

    @staticmethod
    def get_cell_color(cell: Cell) -> str:
        if cell.is_targeted:
            fill = EnemyBoardFrame.get_targeted_cell_color(cell)
        else:
            fill = config.EMPTY_COLOR
        return fill

    @staticmethod
    def get_ship_color(ship: Ship) -> str:
        if ship.health == 0:
            fill = config.DESTROYED_COLOR
        else:
            fill = config.HIT_COLOR
        return fill

    @staticmethod
    def get_targeted_cell_color(cell: Cell) -> str:
        if cell.has_ship_part():
            fill = EnemyBoardFrame.get_ship_color(cell.ship)
        else:
            fill = config.MISS_COLOR
        return fill
