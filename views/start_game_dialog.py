import tkinter as tk
from tkinter.messagebox import showinfo

import config


class StartGameDialog(tk.Toplevel):
    DEFAULT_PLAYER_1_NAME: str = "Player 1"
    DEFAULT_PLAYER_2_NAME: str = "Player 2"
    player_1_name: tk.StringVar
    player_2_name: tk.StringVar
    play_against_ai: tk.BooleanVar

    def __init__(self, master, **kw):
        super(StartGameDialog, self).__init__(**kw)
        self.master = master
        self.resizable(0, 0)

        self.player_1_name = tk.StringVar()
        self.player_2_name = tk.StringVar()
        self.play_against_ai = tk.BooleanVar()

    def render(self) -> None:
        self.title("Play Battleship")
        self.render_inputs()
        self.render_submit_button()
        self.render_show_rules_button()
        self.protocol("WM_DELETE_WINDOW", self.cancel)
        self.wait_window(self)

    def render_inputs(self) -> None:
        body = tk.Frame(self)

        tk.Label(body, text="Player 1 name: ").grid(row=1)
        player_1_name = tk.Entry(body, textvariable=self.player_1_name)
        player_1_name.insert(0, self.DEFAULT_PLAYER_1_NAME)
        player_1_name.grid(row=1, column=1, padx=5, pady=5)

        tk.Label(body, text="Player 2 name: ").grid(row=2)
        player_2_name = tk.Entry(body, textvariable=self.player_2_name)
        player_2_name.insert(0, self.DEFAULT_PLAYER_2_NAME)
        player_2_name.grid(row=2, column=1, padx=5, pady=5)

        cb = tk.Checkbutton(body, text="Play against computer", variable=self.play_against_ai)
        cb.grid(row=3, columnspan=2, padx=5)

        body.pack(padx=10)

    def render_submit_button(self) -> None:
        box = tk.Frame(self)
        w = tk.Button(box, text="Play", command=self.play, padx=10)
        w.pack(side=tk.LEFT, padx=10, pady=(5, 5))
        box.pack()

    def render_show_rules_button(self) -> None:
        box = tk.Frame(self)
        w = tk.Button(box, text="Show rules", command=self.show_rules, padx=10)
        w.pack(side=tk.LEFT, padx=10, pady=(0, 5))
        box.pack()

    def play(self) -> None:
        self.master.play = True
        self.validate()
        self.destroy()

    def cancel(self) -> None:
        self.master.play = False
        self.destroy()
        self.master.destroy()

    def validate(self) -> None:
        if not self.player_1_name.get():
            self.player_1_name.set(self.DEFAULT_PLAYER_1_NAME)
        if not self.player_2_name.get():
            self.player_2_name.set(self.DEFAULT_PLAYER_2_NAME)

    def show_rules(self) -> None:
        showinfo("Rules", config.RULES, parent=self)
