from __future__ import annotations

from typing import TYPE_CHECKING

import config
from controllers import placement_board_controller
from views.board_frame import BoardFrame

if TYPE_CHECKING:  # pragma: no cover
    from models.board import Board
    from models.cell import Cell
    from models.player import Player


class PlacementBoardFrame(BoardFrame):
    player: Player

    def __init__(self, board: Board, player: Player, **kw):
        super(PlacementBoardFrame, self).__init__(board, **kw)
        self.player = player

    def bind_cell_events(self, cell: Cell) -> None:
        self.canvas.tag_bind(cell.id, "<Enter>", self.on_enter)
        self.canvas.tag_bind(cell.id, "<Leave>", self.on_leave)
        self.canvas.tag_bind(cell.id, "<Button-1>", self.left_click)
        self.canvas.tag_bind(cell.id, "<Button-3>", self.right_click)

    def on_enter(self, _event=None) -> None:
        cell = self.get_current_cell()
        placement_board_controller.on_enter(cell, self.board)

    def on_leave(self, _event=None) -> None:
        cell = self.get_current_cell()
        placement_board_controller.on_leave(cell, self.board)

    def left_click(self, _event=None) -> None:
        cell = self.get_current_cell()
        placement_board_controller.left_click(cell, self.board, self.player)

    def right_click(self, _event=None) -> None:
        cell = self.get_current_cell()
        placement_board_controller.right_click(cell, self.board)

    def update_cursor(self) -> None:
        current_cell = self.get_current_cell()
        if self.board.is_illegal_placement(current_cell):
            cursor = config.NOT_ALLOWED_CURSOR
        elif self.is_any_cell_hovered():
            cursor = config.HOVER_CURSOR
        else:
            cursor = config.DEFAULT_CURSOR
        self.canvas.config(cursor=cursor)

    def is_any_cell_hovered(self) -> bool:
        current_cell = self.get_current_cell()
        return any([cell == current_cell and cell.is_highlighted for cell in self.board.get_cells()])

    def rerender(self) -> None:
        for cell in self.board.get_cells():
            fill = self.get_cell_color(cell)
            self.canvas.itemconfig(cell.id, fill=fill)
        self.update_cursor()

    @staticmethod
    def get_initial_cell_color(cell: Cell) -> str:
        return config.EMPTY_COLOR

    @staticmethod
    def get_cell_color(cell: Cell) -> str:
        if cell.has_ship_part():
            fill = PlacementBoardFrame.get_cell_with_ship_color(cell)
        elif cell.is_highlighted:
            fill = config.SHIP_HIGHLIGHT_COLOR
        else:
            fill = config.EMPTY_COLOR
        return fill

    @staticmethod
    def get_cell_with_ship_color(cell: Cell) -> str:
        if cell.is_highlighted:
            fill = config.SHIP_OVERLAP_COLOR
        else:
            fill = config.SHIP_COLOR
        return fill
