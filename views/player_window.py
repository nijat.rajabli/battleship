from __future__ import annotations

from tkinter import Tk, Toplevel, messagebox, TclError
from typing import TYPE_CHECKING

from controllers import player_window_controller
from observer import Observer
from views.board_frame import BoardFrame
from views.placement_board_frame import PlacementBoardFrame
from views.ready_board_frame import ReadyBoardFrame

if TYPE_CHECKING:  # pragma: no cover
    from models.player import Player


class PlayerWindow(Observer, Tk, Toplevel):
    player: Player
    own_board_frame: BoardFrame = None
    enemy_board_frame: BoardFrame = None

    def __init__(self, player: Player, master: Toplevel, **kw):
        super(PlayerWindow, self).__init__(**kw)
        self.master = master
        self.player = player

    def render(self) -> None:
        self.observe(self.player, self.rerender)
        self.observe(self.player.enemy, self.rerender)
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.resizable(0, 0)
        self.set_title("Place your ships")

        self.render_own_board_frame()

    def render_own_board_frame(self) -> None:
        self.own_board_frame = PlacementBoardFrame(master=self, board=self.player.board, player=self.player)
        self.own_board_frame.grid(row=0, column=0)
        self.own_board_frame.render()

    def rerender(self) -> None:
        self.update_title()
        if self.player.board.is_ready() and not self.is_placement_board_switched():
            self.switch_placement_board()

    def switch_placement_board(self) -> None:
        ready_board_frame = ReadyBoardFrame(master=self, board=self.player.board)
        ready_board_frame.grid(row=0, column=0)
        self.own_board_frame.destroy()
        self.own_board_frame = ready_board_frame
        self.own_board_frame.render()

    def update_title(self) -> None:
        if not self.player.board.is_ready():
            self.set_title("Place your ships")
        elif self.player.board.is_ready() and not self.player.enemy.board.is_ready():
            self.set_title("Waiting for opponent")
        elif self.player.has_won() or self.player.enemy.has_won():
            self.set_title("Game over")
        elif self.player.has_turn:
            self.set_title("Your turn")
        elif self.player.enemy.has_turn:
            self.set_title("Opponent's turn".format(self.player.enemy.name))

    def is_placement_board_switched(self) -> bool:
        return type(self.own_board_frame) is ReadyBoardFrame

    def set_title(self, message) -> None:
        self.title("{} - {} - Battleship".format(message, self.player.name))

    def on_closing(self) -> None:
        if not self.player.has_won() and self.ask_before_quit():
            self.destroy()
            player_window_controller.on_closing(self.player)

    def ask_before_quit(self) -> bool:
        return messagebox.askokcancel("Quit", "Do you want to quit? You will lose if you quit!", parent=self)

    def destroy(self) -> None:
        self.stop_observing()
        try:
            super(PlayerWindow, self).destroy()
        except TclError:
            pass
