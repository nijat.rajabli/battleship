from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from models.player import Player


def on_closing(player: Player) -> None:
    player.quit()
