from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from models.board import Player
    from models.cell import Cell


def left_click(cell: Cell, current_player: Player) -> None:
    current_player.target_cell(cell)
