from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from models.player import Player
    from models.board import Board
    from models.cell import Cell


def on_enter(cell: Cell, board: Board) -> None:
    board.add_ship_parts_highlight(cell)


def on_leave(cell: Cell, board: Board) -> None:
    board.clear_ship_parts_highlight(cell)


def left_click(cell: Cell, board: Board, player: Player) -> None:
    board.clear_ship_parts_highlight(cell)
    board.place_ship(cell, player)
    board.add_ship_parts_highlight(cell)


def right_click(cell: Cell, board: Board) -> None:
    board.clear_ship_parts_highlight(cell)
    board.toggle_horizontal_ship_placement()
    board.add_ship_parts_highlight(cell)
