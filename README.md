# Battleship game
A battleship game implementation in Python 3 with an option to play against computer.

`Python 3.8` and up is recommended to run the program. Only Python built-in libraries are used, therefore it has no Python dependency. However, if your operating system does not already have [Tk](https://tkdocs.com/tutorial/intro.html) package you will need to install it before running the program. You can run the program by the following command from the project root:
```
python main.py
```
The program is tested on Windows 10, macOS and Ubuntu operating systems. If you get `ModuleNotFoundError: No module named 'Tkinter'` error on Ubuntu, you can install `python3-tk` package:
```
sudo apt-get install python3-tk
```
You can refer to [here](https://tkdocs.com/tutorial/install.html) or [here](https://stackoverflow.com/questions/25905540/importerror-no-module-named-tkinter) if you have problems with `Tkinter` module.

## Game rules
Once the game starts you need to place your ships on the board. By default configuration, each player has 5 ships: one ship with the length of 5, one ship with the length of 4, two ships with the length of 3 and one ship with the length of 2. Use left click of the mouse to place your ship and right click to change the orientation of the ship you are going to place. Ships can touch each other but they cannot overlap. The player who finishes placing his or her ships first gets to make the first move.

You will see your board on the left and opponent's board on the right. In each turn, each player shoots a cell in their opponent's board. After a shot: if the area becomes gray, you have missed the shot; if it becomes dark orange you have hit part of an enemy ship; if it becomes dark red you have sunk an enemy ship. Likewise, you will see your opponent's progress mirrored on your side of the board. Your active ships will be shown in the blue color.

The player who sinks all of their opponent's ships first wins the game.

## Development
### Linting and auto-formatting
Simple "linting" and auto-formatting is done using default *Inspect Code* and *Reformat* tools of [PyCharm](https://www.jetbrains.com/pycharm/), which mostly adhere to [PEP 8 Style Guide](https://www.python.org/dev/peps/pep-0008/). Maximum line length is set to 120.

### Commenting
While writing the methods, a descriptive name is preferred over a comment that describes what the method does. Methods that need comments (except when they explain "why" side of things) are considered a good candidate for refactoring.

### Testing
For unit tests Python's built in [unittest](https://docs.python.org/3/library/unittest.html) framework is used. 

#### Running tests
Tests can be run with the following command from the project root:
```
python -m unittest tests
```
For more command line options please refer to [unittest's documentation](https://docs.python.org/3.3/library/unittest.html#command-line-interface).

#### Writing tests
For writing tests, following conventions that are not specified in `unitest` framework's coding conventions are adopted:
- Order of parameters in `unittest.TestCase.assertEqual` are in the order of *expected* and *actual*. This decision is based on how the used IDE's ([PyCharm](https://www.jetbrains.com/pycharm/)) testing tool understands these parameters by default. 

#### Test coverage
For checking test code coverage, [coverage](https://coverage.readthedocs.io/) tool is used.

To get started install the package:
    ```
    pip install coverage
    ```
To check test coverage:
1. In the project root, run the tests with the following command:
    ```
    coverage run -m unittest discover
    ```
2. Use the following command to get a report on the results:
    ```
    coverage report -m
    ```
More details on how to use the tool is available [here](https://coverage.readthedocs.io/).

If a clause needs to be excluded from the coverage report `# pragma: no cover` comment can be used. In this project this tag is used only within `if TYPE_CHECKING` as this expression is only true when Python type checking is performed on the code, to enable type hints.

### Computer Player
"AI" player is implemented based on the probabilistic approach outlined in [this article](http://www.datagenetics.com/blog/december32011/). 

In the original approach all ships' all possible placements are considered when calculating the possibility of having a ship in certain place, however, in the current implementation ships are placed individually one at a time to calculate probabilities. While, probably, not as good as the original algorithm, this algorithm plays very well, at least against the people who played it.

Another change to the original algorithm is that, in the original algorithm cells around center have the largest weight initially, as cells in the center have more probability of holding a ship. Knowing this fact gives a lot advantage to the human player, as they will avoid center on purpose while placing the ships. For this purpose, a strategy balance variable is introduced. This variable starts with arbitrarily chosen value of 3. If algorithm makes a miss while looking for a cell that has the highest possibility of holding a ship, the value is decreased. When value becomes negative, computer inverts the weights, therefore focusing outside the center. This value alternates during the game based on how good current strategy (look for places where ships are likely or less likely to be placed) is doing.

### Future plans
- Refactoring
    - Further refactor by dividing rather complex methods into multiple smaller methods.
    - Separate set-up and actual test for each test case in `test_main` test suite, so that test cases are easier to follow.
    - Only one custom exception, `IllegalActionException` is defined for custom errors within the game. More customized errors can be defined for each specific case.
- Write more unit tests. After refactoring more tests can be written much faster as it is easier to test smaller methods.
- Improve AI algorithm by checking all ships at once (as mentioned before, currently the algorithm checks possible placement of active ships one by one when calculating the probability of having a ship in a cell)
- One interesting thing to do would be to run simulations with 2 AI players. This way we could get a better idea about average number of moves a game takes and how this compares to the original algorithm. This of course would also allow us to define better parameters for the modified algorithm.