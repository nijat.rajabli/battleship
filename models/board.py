from __future__ import annotations

from typing import List, TYPE_CHECKING, Tuple, Generator

from models.cell import Cell
from observable import Observable

if TYPE_CHECKING:  # pragma: no cover
    from models.player import Player
    from models.ship import Ship


class Board(Observable):
    SIZE: int = 10

    content: List[List[Cell]]
    ships: List[Ship]
    horizontal_ship_placement: bool = True

    def __init__(self, ships: List[Ship]):
        self.content = []
        self.ships = ships
        self.populate_cells()

    def populate_cells(self) -> None:
        for y in range(0, self.SIZE):
            self.content.append([])
            for x in range(0, self.SIZE):
                self.content[y].append(Cell(x, y))

    def is_ready(self) -> bool:
        return self.get_ship_to_place() is None

    def get_ship_to_place(self) -> Ship:
        try:
            ship_to_place = next(ship for ship in self.ships if not ship.is_placed)
        except StopIteration:
            ship_to_place = None
        return ship_to_place

    def add_ship_parts_highlight(self, cell: Cell) -> None:
        self.set_ship_parts_highlight(cell, True)

    def clear_ship_parts_highlight(self, cell: Cell) -> None:
        self.set_ship_parts_highlight(cell, False)

    def set_ship_parts_highlight(self, cell: Cell, state: bool) -> None:
        ship_parts = self.get_ship_parts_to_place(cell)
        for cell in ship_parts:
            cell.is_highlighted = state
        self.notify()

    def toggle_horizontal_ship_placement(self) -> None:
        self.horizontal_ship_placement = not self.horizontal_ship_placement

    def get_ship_parts_to_place(self, cell: Cell) -> List[Cell]:
        ship_parts = []
        ship_to_place = self.get_ship_to_place()
        if ship_to_place:
            ship_parts = self.get_ship_parts(cell, ship_to_place)
        return ship_parts

    def get_ship_parts(self, cell: Cell, ship: Ship) -> List[Cell]:
        start, end = self.get_start_end_indexes_for_ship(cell, ship)
        ship_parts = []
        for i in range(start, end):
            x = i if self.horizontal_ship_placement else cell.x
            y = cell.y if self.horizontal_ship_placement else i
            ship_parts.append(self.content[y][x])
        return ship_parts

    def get_start_end_indexes_for_ship(self, cell: Cell, ship: Ship) -> Tuple[int, int]:
        start = cell.x if self.horizontal_ship_placement else cell.y
        end = start + ship.size
        if end >= self.SIZE:
            start -= ship.size - (self.SIZE - start)
            end = self.SIZE
        return start, end

    def place_ship(self, cell: Cell, player: Player) -> None:
        if not self.is_illegal_placement(cell):
            ship_parts = self.get_ship_parts_to_place(cell)
            ship_to_place = self.get_ship_to_place()
            ship_to_place.place(ship_parts)
            if player.enemy.board.is_ready():
                player.give_turn()
            self.notify()

    def is_illegal_placement(self, cell: Cell) -> bool:
        ship_parts = self.get_ship_parts_to_place(cell)
        return any(cell.has_ship_part() for cell in ship_parts)

    def get_cells(self) -> Generator[Cell]:
        return (cell for rows in self.content for cell in rows)

    def get_active_ships(self) -> List[Ship]:
        return [ship for ship in self.ships if not ship.is_destroyed()]
