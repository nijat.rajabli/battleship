from __future__ import annotations

from typing import List, TYPE_CHECKING

from exceptions.illegal_action_exception import IllegalActionException

if TYPE_CHECKING:  # pragma: no cover
    from models.cell import Cell


class Ship:
    cells: List[Cell]
    size: int
    health: int
    is_placed: bool = False

    def __init__(self, size: int):
        self.size = size
        self.health = self.size

    def is_destroyed(self) -> bool:
        return self.health == 0

    def hit(self) -> None:
        if self.health == 0:
            raise IllegalActionException("Ship cannot be hit when its health is zero")
        self.health -= 1

    def place(self, ship_parts: List[Cell]) -> None:
        self.attach_to_cells(ship_parts)
        self.cells = ship_parts
        self.is_placed = True

    def attach_to_cells(self, ship_parts: List[Cell]) -> None:
        for cell in ship_parts:
            cell.attach_ship(self)
