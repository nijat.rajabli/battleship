from __future__ import annotations

from typing import TYPE_CHECKING

from observable import Observable

if TYPE_CHECKING:  # pragma: no cover
    from models.player import Player


class Game(Observable):
    player_1: Player
    player_2: Player

    def __init__(self, player_1, player_2):
        self.player_1 = player_1
        self.player_2 = player_2

    def is_ready(self) -> bool:
        return self.player_1.board.is_ready() and self.player_2.board.is_ready()

    def is_over(self) -> bool:
        return self.player_1.has_won() or self.player_2.has_won()

    def get_winner_name(self) -> str:
        return self.player_1.name if self.player_1.has_won() else self.player_2.name
