from __future__ import annotations

from typing import TYPE_CHECKING

from observable import Observable

if TYPE_CHECKING:  # pragma: no cover
    from models.board import Board
    from models.cell import Cell


class Player(Observable):
    name: str
    board: Board
    enemy: Player
    has_turn: bool = False
    has_quit: bool = False

    def __init__(self, name: str, board: Board):
        self.name = name
        self.board = board

    def take_turn(self) -> None:
        if not self.enemy.has_turn:
            self.has_turn = True
        self.notify()

    def give_turn(self) -> None:
        self.has_turn = False
        self.enemy.take_turn()
        self.notify()

    def target_cell(self, cell: Cell) -> None:
        if self.can_target_cell(cell):
            cell.target()
            self.enemy.board.notify()
            if self.has_won():
                self.notify()
            else:
                self.give_turn()

    def can_target_cell(self, cell: Cell) -> bool:
        return self.has_turn and not cell.is_targeted

    def has_won(self) -> bool:
        return self.enemy.has_quit or all([ship.is_destroyed() for ship in self.enemy.board.ships])

    def quit(self) -> None:
        self.has_quit = True
        self.notify()

    def is_ai_player(self) -> bool:
        return False

    def set_enemy(self, player: Player) -> None:
        self.enemy = player
