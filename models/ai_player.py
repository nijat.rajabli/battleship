from __future__ import annotations

import random
from operator import attrgetter
from typing import TYPE_CHECKING, List, Tuple

from models.player import Player

if TYPE_CHECKING:  # pragma: no cover
    from models.cell import Cell
    from models.ship import Ship
    from models.board import Board


class AIPlayer(Player):
    hunt_mode: bool = True
    target_mode: bool = False
    hit_cells: List[Cell]
    hunt_highest_probability: bool = True
    hunt_strategy_balance: int = 3

    def __init__(self, name: str, board: Board):
        super(AIPlayer, self).__init__(name, board)
        self.hit_cells = []

    def is_ai_player(self) -> bool:
        return True

    def set_enemy(self, player: Player) -> None:
        super(AIPlayer, self).set_enemy(player)
        self.place_ships()

    def place_ships(self) -> None:
        """Place ships at random until the board is ready"""
        while True:
            x, y = self.decide_random_ship_position()
            cell = self.board.content[y][x]
            if not self.board.is_illegal_placement(cell):
                self.board.place_ship(cell, self)
                if self.board.is_ready():
                    break

    def decide_random_ship_position(self) -> Tuple[int, int]:
        self.board.horizontal_ship_placement = bool(random.getrandbits(1))
        x = random.randrange(10)
        y = random.randrange(10)
        return x, y

    def take_turn(self) -> None:
        super(AIPlayer, self).take_turn()
        if self.enemy.board.is_ready():
            self.make_move()

    def make_move(self) -> None:
        self.reset_cell_weights()
        self.set_cell_weights()
        if self.hunt_mode:
            self.make_move_in_hunt_mode()
        elif self.target_mode:
            self.make_move_in_target_mode()

    def reset_cell_weights(self) -> None:
        for cell in self.enemy.board.get_cells():
            cell.weight = 0

    def set_cell_weights(self) -> None:
        """For each possible placement of each active ship, add weight to the cells ships can be placed"""
        active_ships = self.enemy.board.get_active_ships()
        for cell in self.enemy.board.get_cells():
            for horizontal in [True, False]:
                self.enemy.board.horizontal_ship_placement = horizontal
                for ship in active_ships:
                    self.add_cell_weights_for_placement(cell, ship)

    def add_cell_weights_for_placement(self, cell: Cell, ship: Ship) -> None:
        """Add calculated weight to each cell that can hold the given ship

        Args:
            cell (Cell): Current cell that points to the start position of the ship.
            ship (Ship): Ship that needs to be placed.
        """
        ship_parts = self.enemy.board.get_ship_parts(cell, ship)
        is_illegal_placement = any(cell.is_targeted and not cell.is_hit() for cell in ship_parts)
        if not is_illegal_placement:
            weight = self.calculate_weight_for_placement(ship_parts)
            for ship_part in ship_parts:
                ship_part.weight += weight

    def calculate_weight_for_placement(self, ship_parts: List[Cell]) -> int:
        """Calculate weight for given cells based on the knowledge of hit cells.

        By default weight increment for a cell that can hold the current ship is 1. However,
        if more than one cell of the current ship placement intersect with the hit cells,
        it means that it is likely that the ship we have hit is the current ship we place.
        More cells that intersect means more likelihood of that. For this reason weight is
        incremented by a term that is proportional to the number of intersecting cells.
        This term is also multiplied by an arbitrarily high number to make sure that the
        weight is much larger than default accumulation of weights (in center default
        accumulation of weights can be high for example).

        Args:
            ship_parts (List[Cell]): Cells for the ship that is placed

        Returns:
            int: Calculated weight.
        """
        intersection = set(ship_parts).intersection(self.hit_cells)
        weight = 100 * len(intersection) if len(intersection) > 1 else 1
        return weight

    def make_move_in_hunt_mode(self) -> None:
        cell = self.decide_cell_to_target_in_hunt_mode()
        if not cell.is_targeted:
            self.target_cell(cell)
            self.update_hunt_strategy(cell.is_hit())
            if cell.is_hit():
                self.hit_cells.append(cell)
                self.switch_to_target_mode()

    def update_hunt_strategy(self, is_hit: bool) -> None:
        """Update hunt strategy based on the result of last shot

        If last shot was a hit, then continue incrementing the balance in favor of
        the the current strategy. When 'hunt_strategy_balance' is positive or zero
        the strategy is to hunt cells with the highest probability. Once the balance
        is negative, cells that are less likely to have a ship are targeted. The
        balance in favor of the strategy that hunts by the highest probability is
        incremented by 2 rather than 1, as it is usually a better strategy.

        Args:
            is_hit (bool): whether last shot was a hit or not
        """
        self.hunt_strategy_balance += 2 if not (is_hit ^ self.hunt_highest_probability) else -1
        self.hunt_highest_probability = self.hunt_strategy_balance >= 0

    def decide_cell_to_target_in_hunt_mode(self) -> Cell:
        """Look for a cell to target with even parity restriction.

        This assumes the board to be a checker board and targets a cell only in one color.
        For example, assuming the board starts with a white cell, even parity restriction
        would make computer target white cells only.
        """
        cells = list(self.enemy.board.get_cells())
        while True:
            weights = self.extract_weights_from_cells(cells)
            cell = random.choices(cells, weights)[0]
            if (cell.x + cell.y) % 2 == 0 and not cell.is_targeted:
                return cell
            else:
                cells.remove(cell)

    def extract_weights_from_cells(self, cells: List[Cell]) -> List[int]:
        """Extract weight from cells based on the current strategy.

        If current strategy is to focus on center, then cells are extracted as they are.
        If the current strategy is not to focus on center, the weights of the cells are
        inverted.

        Args:
            cells: Cells to extract the weights from

        Returns:
            List[int]: List of weights for the given cells
        """
        weights = []
        for cell in cells:
            if self.hunt_highest_probability or cell.weight == 0:
                weights.append(cell.weight)
            else:
                weights.append(cell.weight ** -1)
        return weights

    def make_move_in_target_mode(self) -> None:
        cell = self.decide_cell_to_target_in_target_mode()
        self.target_cell(cell)
        if cell.is_hit():
            self.hit_cells.append(cell)
            if cell.ship.is_destroyed():
                self.filter_out_destroyed_ship_cells(cell)
                if not self.hit_cells:
                    self.switch_to_hunt_mode()

    def filter_out_destroyed_ship_cells(self, cell: Cell) -> None:
        self.hit_cells = [c for c in self.hit_cells if c not in cell.ship.cells]

    def decide_cell_to_target_in_target_mode(self) -> Cell:
        cells_to_target = self.get_cells_to_target_in_target_mode()
        cell_to_target = self.get_cell_with_the_largest_weight(cells_to_target)
        return cell_to_target

    @staticmethod
    def get_cell_with_the_largest_weight(cells: List[Cell]):
        return max(cells, key=attrgetter("weight"))

    def get_cells_to_target_in_target_mode(self) -> List[Cell]:
        cells_to_target = []
        for cell in self.hit_cells:
            neighbour_cells = self.get_neighbour_cells(cell.x, cell.y)
            cells_to_target.extend(neighbour_cells)
        return cells_to_target

    def get_neighbour_cells(self, x: int, y: int) -> List[Cell]:
        neighbour_cells = []

        # West neighbour
        if x > 0 and not self.get_enemy_board_cell(x - 1, y).is_targeted:
            neighbour_cells.append(self.get_enemy_board_cell(x - 1, y))
        # East neighbour
        if x < 9 and not self.get_enemy_board_cell(x + 1, y).is_targeted:
            neighbour_cells.append(self.get_enemy_board_cell(x + 1, y))
        # South neighbour
        if y > 0 and not self.get_enemy_board_cell(x, y - 1).is_targeted:
            neighbour_cells.append(self.get_enemy_board_cell(x, y - 1))
        # North neighbour
        if y < 9 and not self.get_enemy_board_cell(x, y + 1).is_targeted:
            neighbour_cells.append(self.get_enemy_board_cell(x, y + 1))

        return neighbour_cells

    def switch_to_target_mode(self) -> None:
        self.target_mode = True
        self.hunt_mode = False

    def switch_to_hunt_mode(self) -> None:
        self.target_mode = False
        self.hunt_mode = True

    def get_enemy_board_cell(self, x: int, y: int) -> Cell:
        return self.enemy.board.content[y][x]
