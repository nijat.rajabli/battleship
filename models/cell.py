from __future__ import annotations

from typing import TYPE_CHECKING

from exceptions.illegal_action_exception import IllegalActionException

if TYPE_CHECKING:  # pragma: no cover
    from models.ship import Ship


class Cell:
    id: int = None
    x: int
    y: int
    ship: Ship = None
    is_highlighted: bool = False
    is_targeted: bool = False
    weight: int = 0

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def has_ship_part(self) -> bool:
        return self.ship is not None

    def is_hit(self) -> bool:
        return self.has_ship_part() and self.is_targeted

    def target(self) -> None:
        if self.is_targeted:
            raise IllegalActionException("Cell cannot be targeted once it is already targeted")
        self.is_targeted = True
        if self.has_ship_part():
            self.ship.hit()

    def attach_ship(self, ship: Ship) -> None:
        self.ship = ship
