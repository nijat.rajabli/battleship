import unittest
from unittest.mock import Mock

from models.game import Game


class TestGame(unittest.TestCase):
    def setUp(self):
        self.mock_player_1 = Mock(name="player_1_instance")
        self.mock_player_2 = Mock(name="player_2_instance")
        self.game = Game(self.mock_player_1, self.mock_player_2)

    def test_constructor(self):
        self.assertEqual(self.mock_player_1, self.mock_player_1)
        self.assertEqual(self.mock_player_2, self.mock_player_2)

    def test_is_ready_when_only_board_of_one_player_is_ready(self):
        self.mock_player_1.board.is_ready.return_value = False
        self.mock_player_2.board.is_ready.return_value = True
        self.assertFalse(self.game.is_ready())

    def test_is_ready_when_boards_of_both_players_are_ready(self):
        self.mock_player_1.board.is_ready.return_value = True
        self.mock_player_1.board.is_ready.return_value = True
        self.assertTrue(self.game.is_ready())

    def test_is_over_when_game_is_not_over(self):
        self.mock_player_1.has_won.return_value = False
        self.mock_player_2.has_won.return_value = False
        self.assertFalse(self.game.is_over())

    def test_is_over_when_game_is_over(self):
        self.mock_player_1.has_won.return_value = False
        self.mock_player_2.has_won.return_value = True
        self.assertTrue(self.game.is_over())

    def test_get_winner_name_when_player_1_has_won(self):
        self.mock_player_1.has_won.return_value = True
        self.assertTrue(self.game.get_winner_name(), self.mock_player_1.name)

    def test_get_winner_name_when_player_2_has_won(self):
        self.mock_player_2.has_won.return_value = True
        self.assertTrue(self.game.get_winner_name(), self.mock_player_2.name)
