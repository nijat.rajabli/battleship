import unittest
from unittest.mock import Mock

from exceptions.illegal_action_exception import IllegalActionException
from models.cell import Cell


class TestCell(unittest.TestCase):
    def setUp(self):
        self.x = 1
        self.y = 2
        self.cell = Cell(self.x, self.y)

    def test_constructor(self):
        self.assertEqual(self.x, self.cell.x)
        self.assertEqual(self.y, self.cell.y)

    def test_has_ship_part_when_cell_has_ship_part(self):
        self.cell.ship = Mock(name="ship_instance")
        self.assertTrue(self.cell.has_ship_part())

    def test_has_ship_part_when_cell_does_not_have_ship_part(self):
        self.cell.ship = None
        self.assertFalse(self.cell.has_ship_part())

    def test_is_hit_when_cell_does_not_have_ship_part_but_is_targeted(self):
        self.cell.ship = None
        self.cell.is_targeted = True
        self.assertFalse(self.cell.is_hit())

    def test_is_hit_when_cell_does_not_have_ship_part_and_is_not_targeted(self):
        self.cell.ship = None
        self.cell.is_targeted = False
        self.assertFalse(self.cell.is_hit())

    def test_is_hit_when_cell_has_ship_part_and_is_targeted(self):
        self.cell.ship = Mock(name="ship_instance")
        self.cell.is_targeted = True
        self.assertTrue(self.cell.is_hit())

    def test_target_when_cell_is_already_targeted(self):
        self.cell.is_targeted = True
        self.assertRaises(IllegalActionException, self.cell.target)

    def test_target_when_cell_does_not_have_ship_part(self):
        self.cell.ship = None
        self.cell.is_targeted = False
        self.cell.target()
        self.assertTrue(self.cell.is_targeted)

    def test_target_when_cell_has_ship_part(self):
        mock_ship_hit = Mock(name="ship_hit_method")
        self.cell.ship = Mock(name="ship_instance", hit=mock_ship_hit)
        self.cell.is_targeted = False
        self.cell.target()
        self.assertTrue(self.cell.is_targeted)
        mock_ship_hit.assert_called_once()

    def test_attach_ship(self):
        mock_ship = Mock(name="ship_instance")
        self.cell.attach_ship(mock_ship)
        self.assertEqual(mock_ship, self.cell.ship)
