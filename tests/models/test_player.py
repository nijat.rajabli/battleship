import unittest
from unittest.mock import Mock, MagicMock, patch

from models.player import Player


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.player_name = "Test player name"
        self.board = Mock(name="own_board_instance")
        self.player = Player(self.player_name, self.board)

    def set_mock_enemy(self):
        self.player.enemy = Mock(name="enemy_instance")

    def test_constructor(self):
        self.assertEqual(self.player_name, self.player_name)
        self.assertEqual(self.board, self.board)

    @patch("models.player.Player.notify", autospec=True)
    def test_take_turn_when_enemy_already_has_taken_turn(self, mock_notify: MagicMock):
        self.set_mock_enemy()
        self.player.enemy.has_turn = True
        self.player.take_turn()
        self.assertFalse(self.player.has_turn)
        mock_notify.assert_called_once()

    @patch("models.player.Player.notify", autospec=True)
    def test_take_turn_when_enemy_has_not_taken_turn(self, mock_notify: MagicMock):
        self.set_mock_enemy()
        self.player.enemy.has_turn = False
        self.player.take_turn()
        self.assertTrue(self.player.has_turn)
        mock_notify.assert_called_once()

    def test_give_turn(self):
        self.set_mock_enemy()
        self.player.give_turn()
        self.assertFalse(self.player.has_turn)
        self.player.enemy.take_turn.assert_called_once()

    def test_set_enemy(self):
        mock_enemy = Mock(name="enemy_instance")
        self.player.set_enemy(mock_enemy)
        self.player.enemy = mock_enemy
