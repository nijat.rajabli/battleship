import unittest
from unittest.mock import Mock, patch, MagicMock, call

from exceptions.illegal_action_exception import IllegalActionException
from models.ship import Ship


class TestShip(unittest.TestCase):
    def test_constructor(self):
        size = 5
        ship = Ship(size)
        self.assertEqual(size, ship.size)
        self.assertEqual(size, ship.health,
                         "Initially health should be equal to the ship size")

    def test_is_destroyed_when_ship_is_destroyed(self):
        ship = Ship(0)
        self.assertTrue(ship.is_destroyed())

    def test_is_destroyed_when_ship_is_not_destroyed(self):
        ship = Ship(3)
        self.assertFalse(ship.is_destroyed())

    def test_hit_raises_assertion_error_when_health_is_zero(self):
        ship = Ship(0)
        self.assertRaises(IllegalActionException, ship.hit)

    def test_hit_decreases_health(self):
        health = 4
        ship = Ship(health)
        ship.hit()
        self.assertEqual(health - 1, ship.health)

    @patch("models.ship.Ship.attach_to_cells", autospec=True)
    def test_ship_place(self, mock_attach_to_cells: MagicMock):
        ship = Ship(2)
        ship_parts = [Mock("cell_instance")] * ship.size
        ship.place(ship_parts)
        self.assertEqual(ship_parts, ship.cells)
        self.assertTrue(ship.is_placed)
        mock_attach_to_cells.assert_called_once_with(ship, ship_parts)

    def test_attach_to_cells(self):
        ship = Ship(3)
        mock_attach_ship = Mock(name="attach_ship_method")
        mock_cell = Mock(name="cell_instance", attach_ship=mock_attach_ship)
        ship_parts = [mock_cell] * ship.size
        ship.attach_to_cells(ship_parts)
        calls = [call(ship)] * ship.size
        mock_attach_ship.assert_has_calls(calls)
