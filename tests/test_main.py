import unittest
from unittest.mock import patch, Mock, MagicMock

import config
import main
from models.ai_player import AIPlayer
from models.board import Board
from models.game import Game
from models.player import Player


class TestMain(unittest.TestCase):
    @patch("main.StartGameDialog", autospec=True)
    def test_create_start_game_dialog(self, mock_start_game_dialog_class: MagicMock):
        mock_master = Mock(name="master")

        start_game_dialog = main.create_start_game_dialog(mock_master)

        mock_start_game_dialog_class.assert_called_once_with(master=mock_master)
        mock_start_game_dialog_class.return_value.render.assert_called_once()
        self.assertEqual(mock_start_game_dialog_class.return_value, start_game_dialog)

    def test_create_board(self):
        board = main.create_board()
        self.assertIsInstance(board, Board)
        ship_sizes = [ship.size for ship in board.ships]
        self.assertEqual(config.SHIP_SIZES, ship_sizes)

    @patch("main.AIPlayer.place_ships", autospec=True)
    @patch("main.create_board", autospec=True)
    def test_create_players(self, mock_create_board: MagicMock, mock_ai_player_place_ships: MagicMock):
        player_1_name = "Test Player 1"
        player_2_name = "Test player 2"
        for play_against_ai in [True, False]:
            with self.subTest(play_against_ai=play_against_ai):
                player_1, player_2 = main.create_players(player_1_name, player_2_name, play_against_ai)

                self.assertIsInstance(player_1, Player)
                if play_against_ai:
                    self.assertIsInstance(player_2, AIPlayer)
                    mock_ai_player_place_ships.assert_called_once()
                else:
                    self.assertIsInstance(player_2, Player)
                self.assertEqual(player_1_name, player_1.name)
                self.assertEqual(player_2, player_1.enemy)
                self.assertEqual(mock_create_board.return_value, player_1.board)
                self.assertEqual(player_1, player_2.enemy)
                self.assertEqual(player_2_name, player_2.name)
                self.assertEqual(mock_create_board.return_value, player_2.board)

    @patch("main.create_players", autospec=True)
    def test_create_game(self, mock_create_players: MagicMock):
        player_1 = Mock(name="player_1_instance")
        player_2 = Mock(name="player_2_instance")
        mock_create_players.return_value = (player_1, player_2)
        player_1_name = "Test Player 1"
        player_2_name = "Test player 2"
        play_against_ai = False

        game = main.create_game(player_1_name, player_2_name, play_against_ai)

        self.assertIsInstance(game, Game)
        self.assertEqual(game.player_1, player_1)
        self.assertEqual(game.player_2, player_2)
        mock_create_players.assert_called_once_with(player_1_name, player_2_name, play_against_ai)

    @patch("main.GameWindow", autospec=True)
    def test_open_game_window(self, mock_game_window_class: MagicMock):
        mock_game = Mock(name="game")
        mock_master = Mock(name="master")

        main.open_game_window(mock_game, mock_master)

        mock_game_window_class.assert_called_once_with(mock_game, master=mock_master)
        mock_game_window_class.return_value.render.assert_called_once()
