import unittest
from unittest.mock import Mock, call

from controllers import placement_board_controller


class TestPlacementBoardController(unittest.TestCase):
    def test_on_enter(self):
        mock_cell = Mock(name="cell_instance")
        mock_board = Mock(name="board_instance")
        placement_board_controller.on_enter(mock_cell, mock_board)
        mock_board.add_ship_parts_highlight.assert_called_once_with(mock_cell)

    def test_on_leave(self):
        mock_cell = Mock(name="cell_instance")
        mock_board = Mock(name="board_instance")
        placement_board_controller.on_leave(mock_cell, mock_board)
        mock_board.clear_ship_parts_highlight.assert_called_once_with(mock_cell)

    def test_left_click(self):
        mock_cell = Mock(name="cell_instance")
        mock_board = Mock(name="board_instance")
        mock_player = Mock(name="player_instance")

        placement_board_controller.left_click(mock_cell, mock_board, mock_player)

        mock_board.assert_has_calls([
            call.clear_ship_parts_highlight(mock_cell),
            call.place_ship(mock_cell, mock_player),
            call.add_ship_parts_highlight(mock_cell)
        ])

    def test_right_click(self):
        mock_cell = Mock(name="cell_instance")
        mock_board = Mock(name="board_instance")

        placement_board_controller.right_click(mock_cell, mock_board)

        mock_board.assert_has_calls([
            call.clear_ship_parts_highlight(mock_cell),
            call.toggle_horizontal_ship_placement(),
            call.add_ship_parts_highlight(mock_cell)
        ])
