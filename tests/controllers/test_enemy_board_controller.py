import unittest
from unittest.mock import Mock

from controllers import enemy_board_controller


class TestEnemyBoardController(unittest.TestCase):
    def test_left_click(self):
        mock_cell = Mock(name="cell_instance")
        mock_player = Mock(name="player_instance")
        enemy_board_controller.left_click(mock_cell, mock_player)
        mock_player.target_cell.assert_called_once_with(mock_cell)
