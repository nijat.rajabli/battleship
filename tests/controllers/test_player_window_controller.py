import unittest
from unittest.mock import Mock

from controllers import player_window_controller


class TestPlayerWindowController(unittest.TestCase):
    def test_left_click(self):
        mock_player = Mock(name="player_instance")
        player_window_controller.on_closing(mock_player)
        mock_player.quit.assert_called_once()
