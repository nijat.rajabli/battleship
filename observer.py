from __future__ import annotations

from typing import Dict, Callable, TYPE_CHECKING, List

if TYPE_CHECKING:  # pragma: no cover
    from observable import Observable


class Observer:
    observers: List[Observer] = []
    observables: Dict[Observable, Callable]

    def __init__(self, **kw):
        super(Observer, self).__init__(**kw)
        self.observers.insert(0, self)
        self.observables = {}

    def stop_observing(self) -> None:
        if self in self.observers:
            self.observers.remove(self)

    def observe(self, observable: Observable, callback: Callable) -> None:
        self.observables[observable] = callback
