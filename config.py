DEFAULT_CURSOR = "arrow"
NOT_ALLOWED_CURSOR = "X_cursor"
HOVER_CURSOR = "hand2"
TARGET_CURSOR = "target"

SHIP_COLOR = "#009ACD"
SHIP_HIGHLIGHT_COLOR = "#62AFC8"
SHIP_OVERLAP_COLOR = "#00688B"
EMPTY_COLOR = "#FFFFFF"
DESTROYED_COLOR = "#B20000"
HIT_COLOR = "#B25900"
MISS_COLOR = "#CCCCCC"

SHIP_SIZES = [5, 4, 3, 3, 2]

RULES = "Once the game starts you need to place your ships on the board. Each player has 5 ships: one " \
        "ship with the length of 5, one ship with the length of 4, two ships with the length of 3 and " \
        "one ship with the length of 2. Use left click of the mouse to place your ship and right click " \
        "to change the orientation of the ship. Ships can touch each other but they cannot overlap. The" \
        "player who finishes placing his or her ships first gets to make the first move. \n\n" \
        "You will see your board on the left and opponent's board on the right. In each turn, each " \
        "player shoots a cell in their opponent's board. After a shot: if the area becomes gray, you have " \
        "missed the shot; if it becomes dark orange you have hit part of an enemy ship; if it becomes dark " \
        "red you have sunk an enemy ship. Likewise, you will see your opponent's progress mirrored on " \
        "your side of the board. Your active ship will be shown in the blue color. \n\n" \
        "The objective is to sink all your opponent's ships first."
