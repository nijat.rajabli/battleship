from observer import Observer


class Observable:
    def notify(self) -> None:
        for observer in Observer.observers:
            if self in observer.observables:
                observer.observables[self]()
