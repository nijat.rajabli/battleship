from tkinter import Tk
from typing import Tuple

import config
from models.ai_player import AIPlayer
from models.board import Board
from models.game import Game
from models.player import Player
from models.ship import Ship
from views.game_window import GameWindow
from views.start_game_dialog import StartGameDialog


def create_start_game_dialog(master: Tk) -> StartGameDialog:
    start_game_dialog = StartGameDialog(master=master)
    start_game_dialog.render()
    return start_game_dialog


def create_board() -> Board:
    ships = [Ship(size) for size in config.SHIP_SIZES]
    return Board(ships)


def create_players(player_1_name: str, player_2_name: str, play_against_ai: bool) -> Tuple[Player, Player]:
    player_1_board = create_board()
    player_1 = Player(player_1_name, player_1_board)

    player_2_board = create_board()
    if play_against_ai:
        player_2 = AIPlayer(player_2_name, player_2_board)
    else:
        player_2 = Player(player_2_name, player_2_board)

    player_1.set_enemy(player_2)
    player_2.set_enemy(player_1)

    return player_1, player_2


def create_game(player_1_name: str, player_2_name: str, play_against_ai: bool) -> Game:
    player_1, player_2 = create_players(player_1_name, player_2_name, play_against_ai)
    game = Game(player_1, player_2)
    return game


def open_game_window(game: Game, master: Tk) -> None:
    game_window = GameWindow(game, master=master)
    game_window.render()


def start() -> None:
    while True:
        root = Tk()
        root.play = True
        root.withdraw()

        dialog = create_start_game_dialog(root)
        if not root.play:
            break

        game = create_game(dialog.player_1_name.get(), dialog.player_2_name.get(), dialog.play_against_ai.get())
        open_game_window(game, root)

        root.mainloop()
        if not root.play:
            break


if __name__ == "__main__":
    start()
